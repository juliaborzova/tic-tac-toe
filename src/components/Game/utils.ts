export const sumOfCell = (elements: (number | null)[]) => {
    return elements.reduce((accumulator, item) => {
        if (item === null || accumulator === null) {
            return null;
        }
        return (
            accumulator + item
        )
    }, 0)
}

export const checkwinner = (summ: (number | null)) => {
    if (summ === 0) {
        return 'circle'
    }
    else if (summ === 3) {
        return 'cross'
    }
    return null
}