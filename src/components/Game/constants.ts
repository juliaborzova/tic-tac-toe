export interface IPosition {
    X: number;
    Y: number;
}

export type TGameState = (null | number)[][];

export const gameFieldDefault: TGameState = [
    [null, null, null],
    [null, null, null],
    [null, null, null]
]