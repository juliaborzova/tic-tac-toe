import { useEffect, useState } from 'react'
import classNames from 'classnames'

import { Toast } from '@/components/Toast/Toast';

import { gameFieldDefault, IPosition, TGameState } from './constants';
import { checkwinner, sumOfCell } from './utils';

import styles from './Game.module.scss'

export const Game = () => {
    const [gameState, setGameState] = useState<TGameState>(JSON.parse(JSON.stringify(gameFieldDefault)))
    const [player, setPlayer] = useState<number>(1)
    const [winnerPositions, setWinnerPositions] = useState<IPosition[]>([])
    const [winner, setWinner] = useState<null | string>(null)

    const handleTurn = (row: number, cell: number) => (): void => {
        const newGameState = JSON.parse(JSON.stringify(gameState))

        if (newGameState[row][cell] === null) {
            newGameState[row][cell] = player
            setGameState(newGameState)
            setPlayer(1 - player)
        }
        else {
            alert("Клетка занята!")
        }
    }

    const checkCoordsCombination = (positons: IPosition[]): boolean => {
        let values = [] as (number | null)[];
        positons.forEach((position) => {
            values.push(gameState[position.Y][position.X])
        })

        const summ = sumOfCell(values);
        const winnerFounded = checkwinner(summ)

        if (!!winnerFounded) {
            setWinnerPositions(positons)
            setWinner(winnerFounded)
        }

        return !!winnerFounded;
    }

    useEffect(() => {
        let winnerFounded = false
        gameState.forEach((_, rowIndex) => {
            if (winnerFounded) {
                return
            }

            winnerFounded = checkCoordsCombination([
                {
                    X: 0,
                    Y: rowIndex
                },
                {
                    X: 1,
                    Y: rowIndex
                },
                {
                    X: 2,
                    Y: rowIndex
                }
            ])
        });

        if (winnerFounded) {
            return
        }

        for (let i = 0; i < gameState.length; i++) {
            if (winnerFounded) {
                return
            }
            winnerFounded = checkCoordsCombination([
                {
                    X: i,
                    Y: 0
                },
                {
                    X: i,
                    Y: 1
                },
                {
                    X: i,
                    Y: 2
                }
            ])
        }

        if (winnerFounded) {
            return
        }

        const arrDiagonalFirst: IPosition[] = []
        for (let i = 0; i < gameState.length; i++) {
            arrDiagonalFirst.push({
                X: i,
                Y: i
            })
        }
        winnerFounded = checkCoordsCombination(arrDiagonalFirst)
        if (winnerFounded) {
            return
        }

        const arrDiagonalSecond: IPosition[] = []
        for (let i = 0; i < gameState.length; i++) {
            arrDiagonalSecond.push({
                X: (gameState.length - 1) - i,
                Y: i
            })
        }
        winnerFounded = checkCoordsCombination(arrDiagonalSecond)
    }, [gameState])

    const handleRestart = () => {
        setGameState(gameFieldDefault)
        setWinnerPositions([])
        setWinner(null)
        setPlayer(1)
    }

    return (
        <>
            <main
                className={classNames(styles.game, {
                    [styles.game_blueTurn]: player,
                })}
            >
                {gameState.map((row, rowIndex) => {
                    return (
                        <div
                            key={rowIndex}
                            className={styles.game__stroke}
                        >
                            {row.map((cell, cellIndex) => {
                                return (
                                    <div
                                        key={cellIndex}
                                        className={classNames(styles.game__strokeItem, {
                                            [styles.game__strokeItem_disabled]: cell !== null,
                                            [styles.game__strokeItem_winner]: winnerPositions.find((winPos) => 
                                                winPos.X === cellIndex && winPos.Y === rowIndex
                                            )
                                        })}
                                        onClick={handleTurn(rowIndex, cellIndex)}
                                    >
                                        {(cell === 1) && (<img src="/images/cancel.svg" alt="1" />)}
                                        {(cell === 0) && (<img src="/images/Zero.svg" alt="0" />)}
                                    </div>
                                )
                            })}
                        </div>
                    )
                })}
                <button
                    className={styles.game__button}
                    onClick={handleRestart}
                >
                    Начать заново
                </button>
                {winner !== null && ((winner == "cross") ? (<Toast text="Выиграл крестик" />) : (<Toast text="Выиграл нолик" />))}
            </main>
        </>
    )
}
