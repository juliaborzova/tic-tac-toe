import { FC } from 'react'

import styles from "./Toast.module.scss"

interface IProps {
    text: string
}

export const Toast: FC<IProps> = ({
    text
}) => {
    return (
        <div className={styles.toast}>
            <span>{text}</span>
        </div>
    )
}